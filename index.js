console.log("hi");

class Student {
	constructor(name, email, grades){
		this.name = name;
		this.email = email;
		if(grades.length === 4){
			if(grades.every(grade => grade >= 0 && grade <= 100)){
				this.grades = grades;
			} else {
				this.grades = undefined;
			}
		} else {
			this.grades = undefined;
		}

		this.gradeAve = undefined;
		this.passed = undefined;
		this.passedWithHonors = undefined;
	}
	
	login(){
		console.log(`${this.email} has logged in`);
		return this;
	}

	logout(email){
    	console.log(`${this.email} has logged out`);
    	return this;
	}

	listGrades(grades){
   	 this.grades.forEach(grade => {
        console.log(`${this.name}'s quarterly grade averages are: ${grade}`);
    	})
   	 	return this;

	}

	computeAve(){
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		this.gradeAve = sum / 4;
		return this;
	}

	willPass(){
		this.passed = this.computeAve().gradeAve >= 85 ? true : false;
		return this;
	}

	willPassWithHonors(){
		if(this.passed){
			if(this.gradeAve >= 90){
				this.passedWithHonors = true;
			} else {
				this.passedWithHonors = false;
			}
		} else {
			this.passedWithHonors = false;
		}
		return this;
	}
}

class Section {
	constructor(name){
		// every instantiated Section object will be have an empty array for its students
		this.name = name;
		this.students = [];
		this.honorStudents = undefined;
		this.honorsPercentage = undefined;
	}

	// method for adding student to this section
	// this take in the same arguments needed to instantiate a Student object
	addStudent(name, email, grades){
		// a Student object will be instantiated before being pushed to the students property
		this.students.push(new Student(name, email, grades));
		// return the Section object afterwards, allowing us to chain this method
		return this;
	}
	// method for computing how many students in the section are honor students
	countHonorStudents(){
		let count = 0;
		// remember that each student here is an object instantiated from the Student class
		this.students.forEach(student => {
			if(student.computeAve().willPass().willPassWithHonors().passedWithHonors){
				count++;	
			}
		})
		this.honorStudents = count;
		return this;
	}

	computeHonorsPercentage(){
		this.honorsPercentage = (this.honorStudents / this.students.length) * 100;
		return this;
	}
}
// instantiate a Section object
// const section1A = new Section("section1A");
// console.log(section1A);

// // Section1A
// let studentOne = section1A.addStudent("Tony", "starksindustries@mail.com", [89, 84, 78, 88]);
// let studentTwo = section1A.addStudent("Peter", "spideyman@mail.com", [78, 82, 79, 85]);
// let studentThree = section1A.addStudent("Wanda", "scarlettMaximoff@mail.com", [87, 89, 91, 93]);
// let studentFour = section1A.addStudent("Steve", "captainRogers@mail.com", [91, 89, 92, 93]);

// // Section1B
// section1B.addStudent('Jeremy', 'jeremy@mail.com', [85, 82, 83, 89]);
// section1B.addStudent('Johnny', 'johnny@mail.com', [82, 86, 77, 88]);
// section1B.addStudent('Jerome', 'jerome@mail.com', [89, 85, 92, 91]);
// section1B.addStudent('Janine', 'janine@mail.com', [90, 87, 94, 91]);

// // Section1C
// section1C.addStudent('Faith', 'faith@mail.com', [87, 85, 88, 91]);
// section1C.addStudent('Hope', 'hope@mail.com', [85, 87, 84, 89]);
// section1C.addStudent('Love', 'love@mail.com', [91, 87, 90, 88]);
// section1C.addStudent('Joy', 'joy@mail.com', [92, 86, 90, 89]);

// // Section1D
// section1D.addStudent('Eddie', 'eddie@mail.com', [85, 87, 86, 92]);
// section1D.addStudent('Ellen', 'ellen@mail.com', [88, 84, 86, 90]);
// section1D.addStudent('Edgar', 'edgar@mail.com', [90, 89, 92, 86]);
// section1D.addStudent('Eileen', 'eileen@mail.com', [90, 88, 93, 84]);



/*
	Mini Activity
*/

class Grade {
	constructor(level){
		this.level = level;
		this.sections = [];
		this.totalStudents = 0;
		this.totalHonorStudents = 0;
		this.batchAveGrade = undefined;
		this.batchMinGrade = undefined;
		this.batchMaxGrade = undefined;
	}

	addSection(name){
		this.sections.push(new Section(name));
		return this;
	}

	countStudents(){
		this.sections.forEach(section => this.totalStudents = this.totalStudents + section.students.length);
		return this;
	}

	countHonorStudents(){
		let count = 0;
		this.sections.forEach(section => {
			section.students.forEach(student=> {
				if(student.computeAve().willPass().willPassWithHonors().passedWithHonors){
					count++;
				}
			})
		})
		this.totalHonorStudents = count;
		return this;
	}

	// Activity #4
	// 1.
	computeBatchAve(){
		let ave = 0;
		this.sections.forEach(section => {
			section.students.forEach(student => {
				ave += student.computeAve().gradeAve;
			})
		});
		ave /= this.countStudents().totalStudents;
		this.batchAveGrade = ave;
		return this;
	}

	// 2.
	getBatchMinGrade(){
		let listOfGrades = [];
		this.sections.forEach(section => {
			section.students.forEach(student => {
				student.grades.forEach(grade => {
					listOfGrades.push(grade);
				})
			})
		});
		this.batchMinGrade = listOfGrades.sort((a,b) => a-b)[0];
		return this;
	}

	// 3.
	getBatchMaxGrade(){
		let listOfGrades = [];
		this.sections.forEach(section => {
			section.students.forEach(student => {
				student.grades.forEach(grade => {
					listOfGrades.push(grade);
				})
			})
		});
		this.batchMaxGrade = listOfGrades.sort((a,b) => b-a)[0];
		return this;
	}

}

const grade1 = new Grade(1);
console.log(grade1);

// populate the grade level with sections
grade1.addSection("section1A");
grade1.addSection("section1B");
grade1.addSection("section1C");
grade1.addSection("section1D");

const section1A = grade1.sections.find(section => section.name === "section1A");
const section1B = grade1.sections.find(section => section.name === "section1B");
const section1C = grade1.sections.find(section => section.name === "section1C");
const section1D = grade1.sections.find(section => section.name === "section1D");

// populate the sections with students
// Section1A
section1A.addStudent("Tony", "starksindustries@mail.com", [89, 84, 78, 88]);
section1A.addStudent("Peter", "spideyman@mail.com", [78, 82, 79, 85]);
section1A.addStudent("Wanda", "scarlettMaximoff@mail.com", [87, 89, 91, 93]);
section1A.addStudent("Steve", "captainRogers@mail.com", [91, 89, 92, 93]);

// Section1B
section1B.addStudent('Jeremy', 'jeremy@mail.com', [85, 82, 83, 89]);
section1B.addStudent('Johnny', 'johnny@mail.com', [82, 86, 77, 88]);
section1B.addStudent('Jerome', 'jerome@mail.com', [89, 85, 92, 91]);
section1B.addStudent('Janine', 'janine@mail.com', [90, 87, 94, 91]);

// Section1C
section1C.addStudent('Faith', 'faith@mail.com', [87, 85, 88, 91]);
section1C.addStudent('Hope', 'hope@mail.com', [85, 87, 84, 89]);
section1C.addStudent('Love', 'love@mail.com', [91, 87, 90, 88]);
section1C.addStudent('Joy', 'joy@mail.com', [92, 86, 90, 89]);

// Section1D
section1D.addStudent('Eddie', 'eddie@mail.com', [85, 87, 86, 92]);
section1D.addStudent('Ellen', 'ellen@mail.com', [88, 84, 86, 90]);
section1D.addStudent('Edgar', 'edgar@mail.com', [90, 89, 92, 86]);
section1D.addStudent('Eileen', 'eileen@mail.com', [90, 88, 93, 84]);
